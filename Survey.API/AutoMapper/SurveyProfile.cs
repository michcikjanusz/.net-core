﻿using AutoMapper;
using Survey.API.Models;
using Survey.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Survey.Data
{
    public class SurveyProfile : Profile
    {
        public SurveyProfile()
        {
            this.CreateMap<Question, QuestionModel>()
                .ReverseMap();
            this.CreateMap<Answer, AnswerModel>()
                .ReverseMap();

        }
    }
}
