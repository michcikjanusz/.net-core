﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Survey.API.Models;
using Survey.Data;
using Survey.Domain;

namespace Survey.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class AnswerController : ControllerBase
    {
        private readonly ISurveyRepository _surveyRepository;
        private readonly ILogger<AnswerController> _logger;
        private readonly IMapper _mapper;

        public AnswerController(ISurveyRepository surveyRepository, ILogger<AnswerController> logger, IMapper mapper)
        {
            _surveyRepository = surveyRepository;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]

        public async Task<ActionResult> Delete(int id)
        {
            await _surveyRepository.DeleteAnswer(id);

            if (await _surveyRepository.Commit() > 0)
            {
                return Ok($"Answer with following id {id} had been delted");
            }
            return NotFound(id);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Route("PostChoose")]

        public async Task<ActionResult<Answer>> PostChoose(AnswerModel answer)
        {
            var newAnswer = _mapper.Map<Answer>(answer);
            newAnswer = _surveyRepository.AddAnswerbyQuestion(newAnswer, answer.IdQuestion);
            if (await _surveyRepository.Commit() > 0)
            {
                return Created($"api/answer/{newAnswer.Id}", newAnswer);
            }
            return BadRequest(newAnswer);


        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Route("PostSelected")]

        public async Task<ActionResult<Question>> PostSelected(AnswerModel answer)
        {
            var newAnswer = _mapper.Map<Answer>(answer);
            newAnswer.Selected = true;
            newAnswer = _surveyRepository.AddAnswerbyQuestion(newAnswer, answer.IdQuestion);

            if (await _surveyRepository.Commit() > 0)
            {
                return Created($"api/answer/{newAnswer.Id}", newAnswer);
            }
            return BadRequest(newAnswer);
        }
    }
}