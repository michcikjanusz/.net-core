﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Survey.API.Models;
using Survey.Data;
using Survey.Domain;

namespace Survey.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuestionController : ControllerBase
    {
        private readonly ISurveyRepository _surveyRepository;
        private readonly ILogger<QuestionController> _logger;
        private readonly IMapper _mapper;

        public QuestionController(ISurveyRepository surveyRepository, ILogger<QuestionController> logger, IMapper mapper)
        {
            _surveyRepository = surveyRepository;
            _logger = logger;
            _mapper = mapper;
        }
        
        // GET: api/Question/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Question>> Get(int id)
        {
            var question = await _surveyRepository.GetQuestionbyID(id);
            question.Answers = await _surveyRepository.GetAnswersbyQuestion(id);
            if(question == null)
            {
                return NotFound(id);
            }
            return Ok(question);
        }

        // POST: api/Question
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Question>> Post(QuestionModel question)
        {
            var newQuestion = _mapper.Map<Question>(question);
            newQuestion =  _surveyRepository.AddQuestion(newQuestion);
            if (await _surveyRepository.Commit() > 0)
            {
                return Created($"api/question/{newQuestion.IdQuestion}", newQuestion);
            }
            return BadRequest(newQuestion);

           
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Delete(int id)
        {
            await _surveyRepository.DeleteQuestion(id);

            if (await _surveyRepository.Commit() > 0)
            {
                return Ok($"Question with following id {id} had been delted");
            }
            return NotFound(id);
        }
    }
}
