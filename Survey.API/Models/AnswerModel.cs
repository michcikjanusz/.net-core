﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Survey.API.Models
{
    public class AnswerModel
    {
        [Required]
        public string AnswerName { get; set; }
        [Required]
        public string AnswerType { get; set; }
        [Required(ErrorMessage ="You need specify id of question")]
        public int IdQuestion { get; set; }
    }
}
