﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Survey.API.Models
{
    public class QuestionModel
    {
        [Required]

        public string QuestionName { get; set; }
        [Required]
        public string QuestionType { get; set; }
    }
}
