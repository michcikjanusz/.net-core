﻿using Survey.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Survey.Data
{
    public interface ISurveyRepository 
    {
        Task<Question> GetQuestionbyID(int IdQuestion);
        Task<List<Answer>> GetAnswersbyQuestion(int IdQuestion);

        Answer AddAnswerbyQuestion(Answer answer,int IdQuestion);

        Task<Answer> GetAnswerByID(int idAnswer);

        Question AddQuestion(Question question);

        Task<List<Answer>> GetSelectedAnswers(int idQuestion);

        Task<Answer> DeleteAnswer(int idAnswer);

        Task<Question> DeleteQuestion(int idQuestion);

        Task<int> Commit();

        
    }
}
