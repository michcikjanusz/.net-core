﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Survey.Data.Migrations.Survey
{
    public partial class SelectedinAnswer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Selected",
                table: "Answers",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Selected",
                table: "Answers");
        }
    }
}
