﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Survey.Data.Migrations.Survey
{
    public partial class NEWradioid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SelectedAnswersId",
                table: "Questions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SelectedAnswersId",
                table: "Questions");
        }
    }
}
