﻿using Microsoft.EntityFrameworkCore;
using Survey.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Survey.Data
{
    public class SurveyContext : DbContext
    {
        public SurveyContext(DbContextOptions<SurveyContext> options) : base(options)
        {
        }

        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Question>().HasMany(t => t.Answers)
                        .WithOne(g => g.Question)
                        .HasForeignKey(g => g.IdQuestion);

            modelBuilder.Entity<Answer>()
                .HasKey(k => k.Id);



        }
    }
}
