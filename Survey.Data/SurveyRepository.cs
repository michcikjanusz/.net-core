﻿using Survey.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Survey.Data
{
    public class SurveyRepository : ISurveyRepository
    {
        private readonly SurveyContext _surveyContext;
        private readonly ILogger<SurveyRepository> _logger;

        public SurveyRepository(SurveyContext surveyContext, ILogger<SurveyRepository> logger)
        {
            _surveyContext = surveyContext;
            _logger = logger;
        }
        public Answer AddAnswerbyQuestion(Answer answer, int IdQuestion)
        {
            _logger.LogInformation($"AddAnswerbyQuestion for idQuestion{IdQuestion}");

            answer.IdQuestion = IdQuestion;
            answer.Id = 0;
            _surveyContext.Answers.Add(answer);
            return answer;
        }

        public Question AddQuestion(Question question)
        {
            _logger.LogInformation($"AddQuestion QuestionName: {question.QuestionName}");

            _surveyContext.Questions.Add(question);
            return question;

        }
        public async Task<int> Commit()
        {
            _logger.LogInformation("Commiting");

            return await _surveyContext.SaveChangesAsync();
        }

        public async Task<Answer> DeleteAnswer(int idAnswer)
        {
            _logger.LogInformation($"Deleting answer id {idAnswer}");
            var answer = await GetAnswerByID(idAnswer);

            _surveyContext.Answers.Remove(answer);
            return answer;
        }

        public async Task<Question> DeleteQuestion(int idQuestion)
        {
            _logger.LogInformation($"Deleting Question id {idQuestion}");
            var question = await GetQuestionbyID(idQuestion);

            _surveyContext.Questions.Remove(question);
            return question;
        }

        public async Task<Answer> GetAnswerByID(int idAnswer)
        {
            _logger.LogInformation($"GetAnswerByID : {idAnswer}");

            var answers = _surveyContext.Answers.Where(q => q.Id == idAnswer);
            return await answers.SingleOrDefaultAsync();
        }

        public async Task<List<Answer>> GetAnswersbyQuestion(int IdQuestion)
        {
            _logger.LogInformation($"GetAnswersbyQuestion ID: {IdQuestion}");

            var answers = _surveyContext.Answers.Where(q => q.IdQuestion == IdQuestion && q.Selected == false);
            return await answers.ToListAsync();
        }

        public async Task<Question> GetQuestionbyID(int IdQuestion)
        {
            _logger.LogInformation($"GetQuestionbyID ID: {IdQuestion}");

            return await _surveyContext.Questions.SingleOrDefaultAsync(q => q.IdQuestion == IdQuestion);
        }

        public async Task<List<Answer>> GetSelectedAnswers(int idQuestion)
        {
            _logger.LogInformation($"GetSelectedAnswers for questionID: {idQuestion}");

            var answers =  _surveyContext.Answers.Where(a => a.IdQuestion == idQuestion && a.Selected == true);
            return await answers.ToListAsync();
        }
    }
}
