﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Survey.Domain
{
    public class Answer
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MinLength(3)]
        public string AnswerName { get; set; }
        [Required]
        public AnswerType AnswerType { get; set; }
        public Question Question { get; set; }
        public int IdQuestion { get; set; }
        public bool Selected { get; set; }
    }
}
        