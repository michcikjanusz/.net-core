﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
namespace Survey.Domain
{
    public class Question
    {
        [Key]
        public int IdQuestion { get; set; }
        [Required]
        [MinLength(3)]
        public string QuestionName { get; set; }
        [Required]

        public QuestionType QuestionType { get; set; }

        public List<Answer> Answers { get; set; }
        public int? SelectedAnswersId { get; set; }

    }
}
