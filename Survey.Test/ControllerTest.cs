﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;
using Survey.API.Controllers;
using Survey.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Survey.Test
{
    public class ControllerTest : SurveyConfigurationTest
    {
        [Fact]
        public async Task SetAnswerSelected_Answer_Return201()
        {
            //Arrange
            using (var context = new SurveyContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                var surveyRepo = new SurveyRepository(context, loggerSurvey);
                surveyRepo.AddQuestion(question1);
                await surveyRepo.Commit();
                AnswerController answerController = new AnswerController(surveyRepo, loggerAnswer, mapper);
                // Act
                var result = await answerController.PostSelected(answer3);
                var createdResult = result.Result as ObjectResult;
                var statusCode = createdResult.StatusCode.GetValueOrDefault();
                //assert

                Assert.Equal(201, statusCode);

                context.Database.CloseConnection();
                context.Database.EnsureDeleted();
            }
        }

        [Fact]
        public async Task SetAnswerChoose_Answer_Return201()
        {
            //Arrange
            using (var context = new SurveyContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                var surveyRepo = new SurveyRepository(context, loggerSurvey);
                surveyRepo.AddQuestion(question1);
                await surveyRepo.Commit();
                AnswerController answerController = new AnswerController(surveyRepo, loggerAnswer, mapper);
                // Act
                var result = await answerController.PostChoose(answer3);
                var createdResult = result.Result as ObjectResult;
                var statusCode = createdResult.StatusCode.GetValueOrDefault();
                //assert

                Assert.Equal(201, statusCode);

                context.Database.CloseConnection();
                context.Database.EnsureDeleted();
            }
        }

        [Fact]
        public async Task DeleteAnswer_Answer_Return200()
        {
            //Arrange
            using (var context = new SurveyContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();

                var surveyRepo = new SurveyRepository(context, loggerSurvey);

                surveyRepo.AddQuestion(question1);
                await surveyRepo.Commit();

                var question = await surveyRepo.GetQuestionbyID(1);
                var answer = surveyRepo.AddAnswerbyQuestion(answer1, question.IdQuestion);
                await surveyRepo.Commit();

                AnswerController answerController = new AnswerController(surveyRepo, loggerAnswer, mapper);
                // Act
                var result = await answerController.Delete(answer.Id);
                var createdResult = result as ObjectResult;
                var statusCode = createdResult.StatusCode.GetValueOrDefault();
                //assert
                Assert.Equal(200, statusCode);
                context.Database.CloseConnection();
                context.Database.EnsureDeleted();
            }
        }

        [Fact]
        public async Task SetQuestion_Question_Return201()
        {
            //Arrange
            using (var context = new SurveyContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                var surveyRepo = new SurveyRepository(context, loggerSurvey);
         
                QuestionController questionController = new QuestionController(surveyRepo, loggerQuestion, mapper);
                // Act
                var result = await questionController.Post(question2);
                var createdResult = result.Result as ObjectResult;
                var statusCode = createdResult.StatusCode.GetValueOrDefault();
                //assert

                Assert.Equal(201, statusCode);

                context.Database.CloseConnection();
                context.Database.EnsureDeleted();
            }
        }

        [Fact]
        public async Task DeleteQuestion_Question_Return200()
        {
            //Arrange
            using (var context = new SurveyContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();

                var surveyRepo = new SurveyRepository(context, loggerSurvey);

                surveyRepo.AddQuestion(question1);
                await surveyRepo.Commit();

                QuestionController questionController = new QuestionController(surveyRepo, loggerQuestion, mapper);
                // Act
                var result = await questionController.Delete(1);
                var createdResult = result as ObjectResult;
                var statusCode = createdResult.StatusCode.GetValueOrDefault();

       
                //assert
                Assert.Equal(200, statusCode);
                context.Database.CloseConnection();
                context.Database.EnsureDeleted();
            }
        }

    }
}
