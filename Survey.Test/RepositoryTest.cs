﻿using AutoMapper;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;
using Survey.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Survey.Test
{
    public class RepositoryTest : SurveyConfigurationTest
    {
        [Fact]
        public async Task GetQuestionAsync_question1_ReturnQuestion1()
        {
            //Arrange
            using (var context = new SurveyContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                var surveyRepo = new SurveyRepository(context, loggerSurvey);
                surveyRepo.AddQuestion(question1);
                await surveyRepo.Commit();
                // Act
                var question = await surveyRepo.GetQuestionbyID(1);
                //assert

                Assert.Equal(1, question.IdQuestion);
                Assert.Equal("q1", question.QuestionName);

                context.Database.CloseConnection();
                context.Database.EnsureDeleted();
            }
        }

        [Fact]
        public async Task AddAnswerToQuestion_answer1_ReturnAnswer()
        {
            //Arrange
            using (var context = new SurveyContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                var surveyRepo = new SurveyRepository(context, loggerSurvey);
                surveyRepo.AddQuestion(question1);
                await surveyRepo.Commit();

                var question = await surveyRepo.GetQuestionbyID(1);
                // Act
                var answer = surveyRepo.AddAnswerbyQuestion(answer1,question.IdQuestion);
                await surveyRepo.Commit();

                //assert

                Assert.Equal(1, answer.Id);
                Assert.Equal("a1", answer.AnswerName);
                Assert.True(answer.Selected);

                context.Database.CloseConnection();
                context.Database.EnsureDeleted();
            }
        }

        [Fact]
        public async Task DeleteAnswer_addAnswer_CHeckifEmpty()
        {
            //Arrange
            using (var context = new SurveyContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                var surveyRepo = new SurveyRepository(context, loggerSurvey);
                surveyRepo.AddQuestion(question1);
                await surveyRepo.Commit();

                var question = await surveyRepo.GetQuestionbyID(1);
                // Act
                var answer = surveyRepo.AddAnswerbyQuestion(answer1, question.IdQuestion);
                await surveyRepo.Commit();


                Assert.Equal(1, answer.Id);
                Assert.Equal("a1", answer.AnswerName);
                Assert.True(answer.Selected);

                await surveyRepo.DeleteAnswer(answer.Id);
                await surveyRepo.Commit();

                var deletedAnswer = await surveyRepo.GetAnswerByID(answer.Id);
                //assert

                Assert.Null( deletedAnswer);
                context.Database.CloseConnection();
                context.Database.EnsureDeleted();
            }
        }

    }
}
