﻿using AutoMapper;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;
using Survey.API.Controllers;
using Survey.API.Models;
using Survey.Data;
using Survey.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit.Abstractions;

namespace Survey.Test
{
    public class SurveyConfigurationTest
    {
        public readonly IMapper mapper;
        public readonly ILogger<SurveyRepository> loggerSurvey;
        public readonly ILogger<AnswerController> loggerAnswer;
        public readonly ILogger<QuestionController> loggerQuestion;

        public readonly DbContextOptions<SurveyContext> options;
        public readonly Question question1;
        public readonly QuestionModel question2;
        public readonly Answer answer1;
        public readonly Answer answer2;
        public readonly AnswerModel answer3;

        public SurveyConfigurationTest()
        {
            var myProfile = new SurveyProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            mapper = new Mapper(configuration);

            var mocksurvey = new Mock<ILogger<SurveyRepository>>();
            loggerSurvey = mocksurvey.Object;

            var mockanswer = new Mock<ILogger<AnswerController>>();
            loggerAnswer = mockanswer.Object;

            var mockquestion = new Mock<ILogger<QuestionController>>();
            loggerQuestion = mockquestion.Object;

            var connectionStringBuilder = new SqliteConnectionStringBuilder { DataSource = ":memory:" };
            var connection = new SqliteConnection(connectionStringBuilder.ToString());

            options = new DbContextOptionsBuilder<SurveyContext>()
                .UseSqlite(connection)
                .Options;
            question1 = new Question { QuestionName = "q1", QuestionType = QuestionType.Multi };
            question2 = new QuestionModel { QuestionName = "q2", QuestionType = "Single" };
            answer1 = new Answer { AnswerName = "a1", AnswerType = AnswerType.Closed, Selected = true };
            answer2 = new Answer { AnswerName = "a2", AnswerType = AnswerType.Open, Selected = false };
            answer3 = new AnswerModel { AnswerName = "a3", AnswerType = "closed", IdQuestion = 1 };




        }
    }
}
