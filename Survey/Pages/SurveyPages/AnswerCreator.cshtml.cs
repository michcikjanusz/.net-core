﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Survey.Data;
using Survey.Domain;

namespace Survey
{
    public class AnswerCreatorModel : PageModel
    {
        private readonly IHtmlHelper _htmlHelper;
        private readonly ISurveyRepository _surveyRepository;
        private readonly ILogger<AnswerCreatorModel> _logger;

        public IEnumerable<SelectListItem> AnswerType { get; set; }

        [BindProperty]
        public Answer Answer { get; set; }
        public AnswerCreatorModel(IHtmlHelper htmlHelper, ISurveyRepository surveyRepository, ILogger<AnswerCreatorModel> logger )
        {
            _htmlHelper = htmlHelper;
            _surveyRepository = surveyRepository;
            _logger = logger;
        }
        public IActionResult OnGet(int? IdQuestion)
        {
            _logger.LogInformation($"Get AnswerCreator for Question {IdQuestion}");

            AnswerType = _htmlHelper.GetEnumSelectList<AnswerType>();
            if (!IdQuestion.HasValue)
            {
                return RedirectToPage("./NotFound", new { IdQuestion = IdQuestion });
            }
            return Page();

        }
        public async Task<IActionResult> OnPost(Answer answer, int IdQuestion)
        {
            _logger.LogInformation($"OnPost AnswerCreator for Question {IdQuestion} with AnswerName : {answer.AnswerName}");

            _surveyRepository.AddAnswerbyQuestion(answer, IdQuestion);
            await _surveyRepository.Commit();
            return RedirectToPage("./QuestionCreator", new { IdQuestion = answer.IdQuestion });
        }
        public IActionResult OnPostQuestionCreator(int? IdQuestion)
        {
            _logger.LogInformation("OnPostQuestionCreator");

            return RedirectToPage("./QuestionCreator", new { IdQuestion = IdQuestion });
        }


    }
}