﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Survey.Data;
using Survey.Domain;

namespace Survey
{
    public class DeleteAnswerModel : PageModel
    {
        private readonly ISurveyRepository _surveyRepository;
        private readonly ILogger<DeleteAnswerModel> _logger;

        [BindProperty]
        public Answer Answer { get; set; }
       
        public DeleteAnswerModel(ISurveyRepository surveyRepository, ILogger<DeleteAnswerModel> logger)
        {
            _surveyRepository = surveyRepository;
            _logger = logger;
        }
        public async Task<IActionResult> OnGet(int idAnswer)
        {
            _logger.LogInformation($"Prepering to delete AnswerID {idAnswer}");

            Answer = await  _surveyRepository.GetAnswerByID(idAnswer);

            return Page();

        }

        public async Task<IActionResult> OnPost(int idAnswer)
        {
            Answer = await _surveyRepository.GetAnswerByID(idAnswer);

            _logger.LogInformation($"Deleting AnswerID {Answer.Id}");


            await _surveyRepository.DeleteAnswer(Answer.Id);
            await _surveyRepository.Commit();

             _logger.LogInformation($"Deleted AnswerID {Answer.Id}");

            return RedirectToPage("./QuestionCreator", new { IdQuestion = Answer.IdQuestion });

        }
    }
}