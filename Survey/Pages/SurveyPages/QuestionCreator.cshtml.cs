﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Survey.Data;
using Survey.Domain;

namespace Survey
{
    public class QuestionCreatorModel : PageModel
    {
        private readonly IHtmlHelper _htmlHelper;
        private readonly ISurveyRepository _surveyRepository;
        private readonly ILogger<QuestionCreatorModel> _logger;

        public IEnumerable<SelectListItem> QuestionType { get; set; }
        public IEnumerable<SelectListItem> AnswerType { get; set; }

        [BindProperty]
        public Question Question { get; set; }
        //public IEnumerable<Answer> Answers { get; set; }
        public QuestionCreatorModel(IHtmlHelper htmlHelper, ISurveyRepository surveyRepository, ILogger<QuestionCreatorModel> logger)
        {
            _htmlHelper = htmlHelper;
            _surveyRepository = surveyRepository;
            _logger = logger;
        }
        public async  Task<IActionResult> OnGet(int? IdQuestion)
        {
            _logger.LogInformation($"Geting Question Page for {IdQuestion}");

            QuestionType = _htmlHelper.GetEnumSelectList<QuestionType>();
            AnswerType = _htmlHelper.GetEnumSelectList<AnswerType>();
            if (IdQuestion.HasValue)
            {
                Question = await _surveyRepository.GetQuestionbyID(IdQuestion.Value);
                Question.Answers = await _surveyRepository.GetAnswersbyQuestion(IdQuestion.Value);
            }
            else
            {
                Question = new Question();
                Question.Answers = new List<Answer>();

            }
            if (Question == null)
            {
                return  RedirectToPage("./NotFound", new { IdQuestion = IdQuestion });
            }
            return  Page();
        }
        public async Task<IActionResult> OnPostAnswerCreator()
        {
            _logger.LogInformation($"OnPostAnswerCreator for QuesitionName : {Question.QuestionName}");

            if (Question.IdQuestion == 0)
            {
                 _surveyRepository.AddQuestion(Question);
                await _surveyRepository.Commit();
            }
          
            return  RedirectToPage("./AnswerCreator", new { IdQuestion = Question.IdQuestion });
        }

        public IActionResult OnPostQuestionCreator()
        {
            _logger.LogInformation("OnPostQuestionCreator");

            return RedirectToPage("./QuestionCreator");
        }

        public IActionResult OnPostSave()
        {
            _logger.LogInformation("OnPostSave");

            return RedirectToPage("./Save", new { IdQuestion = Question.IdQuestion });
        }
        

    }
}