﻿using System.Data;
using System.Linq;
using System.Threading.Tasks;
using FusionCharts.DataEngine;
using FusionCharts.Visualization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Survey.Data;
using Survey.Domain;

namespace Survey
{
    public class ResultChartModel : PageModel
    {
        private readonly ISurveyRepository _surveyRepository;
        private readonly ILogger<ResultChartModel> _logger;

        public ResultChartModel(ISurveyRepository surveyRepository, ILogger<ResultChartModel> logger)
        {
            _surveyRepository = surveyRepository;
            _logger = logger;
        }

        [TempData]
        public string Message { get; set; }

        public string ChartJson { get; internal set; }
        public async Task<ActionResult> OnGet(int IdQuestion)
        {
            _logger.LogInformation($"Get Chart for Question id : {IdQuestion}");

            var selectedAnswers = await _surveyRepository.GetSelectedAnswers(IdQuestion);
            var selectedQuestion = await _surveyRepository.GetQuestionbyID(IdQuestion);

            DataTable ChartData = new DataTable();

            ChartData.Columns.Add("Answers", typeof(string));
            ChartData.Columns.Add("Votes", typeof(int));

            var distinctedAnswers = selectedAnswers.GroupBy(x => x.AnswerName).Select(x => x.FirstOrDefault());

            foreach (Answer answer in distinctedAnswers)
            {
                if(answer.AnswerType == AnswerType.Open)
                {
                    ChartData.Rows.Add("Others", selectedAnswers.Where(a => a.AnswerType == AnswerType.Open).Count());
                }
                else
      
                {
                    ChartData.Rows.Add(answer.AnswerName, selectedAnswers.Where(a => a.AnswerName == answer.AnswerName).Count());
                }
            }

            StaticSource source = new StaticSource(ChartData);

            DataModel model = new DataModel();
            model.DataSources.Add(source);
            Charts.ColumnChart column = new Charts.ColumnChart("first_chart");

            column.Width.Pixel(700);
            column.Height.Pixel(400);
            column.Data.Source = model;
            column.Caption.Text = selectedQuestion.QuestionName;
            column.SubCaption.Text = "Results";
            column.Legend.Show = false;
            column.XAxis.Text = "Answers";
            column.YAxis.Text = "Votes";
            column.ThemeName = FusionChartsTheme.ThemeName.FUSION;

            ChartJson = column.Render();
            return Page();
        }
        public  IActionResult OnPostIndex()
        {
            return RedirectToPage("/Index");
        }
    }
}



   