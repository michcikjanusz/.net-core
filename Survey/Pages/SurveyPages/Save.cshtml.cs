﻿
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Survey.Data;
using Survey.Domain;

namespace Survey
{
    public class SaveModel : PageModel
    {
        private readonly IHtmlHelper _htmlHelper;
        private readonly ISurveyRepository _surveyRepository;
        private readonly ILogger<SaveModel> _logger;

        [BindProperty]
        public Question Question { get; set; }
        public Answer Answer { get; set; }

        public SaveModel(IHtmlHelper htmlHelper, ISurveyRepository surveyRepository, ILogger<SaveModel> logger)
        {
            _htmlHelper = htmlHelper;
            _surveyRepository = surveyRepository;
            _logger = logger;
        }

        public async Task<IActionResult> OnGet(int IdQuestion)
        {
            _logger.LogInformation($"Get SavePage for id Question : {IdQuestion}");

            Question = await _surveyRepository.GetQuestionbyID(IdQuestion);
            Question.Answers = await _surveyRepository.GetAnswersbyQuestion(IdQuestion);
            return Page();
        }

        public async Task<IActionResult> OnPostVote()
        {
            _logger.LogInformation($"Post Save for id Question : {Question.IdQuestion}");

            if (Question.QuestionType == QuestionType.Single)
            {
                Answer = await _surveyRepository.GetAnswerByID(Question.SelectedAnswersId.GetValueOrDefault());
                Answer.Selected = true;
                _surveyRepository.AddAnswerbyQuestion(Answer, Answer.IdQuestion);
                await _surveyRepository.Commit();
            }
            else
            {
                foreach (var answer in Question.Answers)
                {
                    if(answer.Selected)
                    {
                        Answer = await _surveyRepository.GetAnswerByID(answer.Id);
                        Answer.Selected = true;
                        _surveyRepository.AddAnswerbyQuestion(Answer, Answer.IdQuestion);
                        await _surveyRepository.Commit();
                    }
                }
            }
            TempData["Message"] = $"You voted for {Answer.AnswerName} !";
            return RedirectToPage("./ResultChart", new { IdQuestion = Answer.IdQuestion });
        }

           

    }
}