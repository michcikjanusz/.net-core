using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Survey.Data;

namespace Survey
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host  = CreateHostBuilder(args).Build();
            using (var scope = host.Services.CreateScope())
            {
                try
                {
                    Log.Information("Starting web Host");
                    var contextSurvey = scope.ServiceProvider.GetService<SurveyContext>();
                    var contextIdentity = scope.ServiceProvider.GetService<ApplicationDbContext>();
                    contextIdentity.Database.Migrate();
                    contextSurvey.Database.Migrate();
                    host.Run();
                }
                catch(Exception ex)
                {
                    Log.Fatal(ex, "Host terminated unexpectedly");
                }
                finally
                {
                    Log.CloseAndFlush();
                }
            }
              
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();

                });
    }
}
